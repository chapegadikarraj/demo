package com.cg.boi.beans;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SchoolStudentResponse {

	@JsonProperty
	private String studentId;

	@JsonProperty
	private String name;
	
	@JsonProperty
	private Set<CourseResponse> courses;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<CourseResponse> getCourses() {
		return courses;
	}

	public void setCourses(Set<CourseResponse> courses) {
		this.courses = courses;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return studentId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return studentId.equals(((SchoolStudentResponse)obj).getStudentId());
	}

}
