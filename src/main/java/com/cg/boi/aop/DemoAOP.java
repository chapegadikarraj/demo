package com.cg.boi.aop;

import java.sql.Timestamp;
import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class DemoAOP {

//	@Autowired
//	private Receiver receiver;
	
	private static Logger LOGGER = LoggerFactory.getLogger(DemoAOP.class);
	
//	@Before("execution(* com.cg.psd.demo1.controllers.*.*(..))")
//	public void demoAOPBefore(){
//		LOGGER.info("Going demo1 controller");
//	}
//	
//	@After("execution(* com.cg.psd.demo1.controllers.*.*(..))")
//	public void demoAOPAfter(){
//		LOGGER.info("Left demo1 controller");
//	}
	
	@Around("execution(* com.cg.psd.demo1.controllers.*.*(..))")
	public Object demoAOPAround(ProceedingJoinPoint joinPoint) throws Throwable {
//		receiver.log("TimeStamp: " + new Timestamp(new Date().getTime())
//				+ " Before method: "+ joinPoint.getSignature().toString());
		LOGGER.info("TimeStamp: " + new Timestamp(new Date().getTime())
				+ " Before method: "+ joinPoint.getSignature().toString());
		Object obj = joinPoint.proceed();
//		receiver.log("TimeStamp: " + new Timestamp(new Date().getTime())
//				+ " After method: "+ joinPoint.getSignature().toString());
		LOGGER.info("TimeStamp: " + new Timestamp(new Date().getTime())
				+ " After method: "+ joinPoint.getSignature().toString());
		return obj;
	}
	
//	@AfterThrowing("execution(* com.cg.psd.demo1.controllers.*.*(..))")
//	public void demoAfterThrowing(Exception ex) throws Throwable {
//		LOGGER.info("TimeStamp: " + new Timestamp(new Date().getTime())
//				+ " Throwing exception in: "+ ex.getMessage());
//	}
}
