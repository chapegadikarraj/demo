package com.cg.boi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="wife")
//@Document(collection="profile")
public class Wife {

	@JsonProperty
	@Id
	@Column(name="wife_id")
	private String wifeId;

	@JsonProperty
	@Column(name="name")
	private String name;
	
//	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "husband_id", updatable = false, insertable = false)
	private Husband husband;

	public String getWifeId() {
		return wifeId;
	}

	public void setWifeId(String wifeId) {
		this.wifeId = wifeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Husband getHusband() {
		return husband;
	}

	public void setHusband(Husband husband) {
		this.husband = husband;
	}

	
	
	
}
