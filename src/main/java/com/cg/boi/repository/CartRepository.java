package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.Cart;


@Repository
public interface CartRepository extends JpaRepository<Cart, String>{
	
	public Cart findByName(String name);
	public List<Cart> findAll(); 

}
