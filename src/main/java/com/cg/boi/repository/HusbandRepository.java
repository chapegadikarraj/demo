package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.Cart;
import com.cg.boi.domain.Husband;


@Repository
public interface HusbandRepository extends JpaRepository<Husband, String>{
	
	public Husband findByName(String name);
	public List<Husband> findAll(); 

}
