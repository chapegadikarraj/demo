package com.cg.boi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.boi.beans.ProfileJson;
import com.cg.boi.domain.Profile;
import com.cg.boi.repository.DemoRepository2;

@Service
public class SaveServiceImpl implements SaveService{

	@Autowired
	private DemoRepository2 repo;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveServiceImpl.class);
	
	
	@Override
	public void save(Profile profile) {
		
		try {
			LOGGER.info("Saving started");
			repo.save(profile);
			LOGGER.info("Saved successfully");
		} catch (Exception e) {
			LOGGER.error("Error while saving profile in table");
			throw new RuntimeException(e.getMessage(), e);
		}
	}

}
