package com.cg.boi.beans;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CourseResponse {

	@JsonProperty
	private String courseId;

	@JsonProperty
	private String name;
	
	@JsonProperty
	private Set<SchoolStudentResponse> students;

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<SchoolStudentResponse> getStudents() {
		return students;
	}

	public void setStudents(Set<SchoolStudentResponse> students) {
		this.students = students;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return courseId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return courseId.equals(((CourseResponse)obj).getCourseId());
	}
}
