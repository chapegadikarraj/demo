package com.cg.boi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="husband")
//@Document(collection="profile")
public class Husband {

	@JsonProperty
	@Id
	@Column(name="husband_id")
	private String husbandId;

	@JsonProperty
	@Column(name="name")
	private String name;
	
	@JsonIgnore
	@OneToOne(mappedBy = "husband")
	private Wife wife;

	public String getHusbandId() {
		return husbandId;
	}

	public void setHusbandId(String husbandId) {
		this.husbandId = husbandId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Wife getWife() {
		return wife;
	}

	public void setWife(Wife wife) {
		this.wife = wife;
	}

	
	
	
}
