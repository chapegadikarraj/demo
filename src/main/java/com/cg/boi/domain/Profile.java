package com.cg.boi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="profile")
//@Document(collection="profile")
public class Profile {

	@JsonProperty
	@Id
	@Column(name="profile_id")
	private String profileId;

	@JsonProperty
	@Column(name="name")
	private String name;
	
	@JsonProperty
	@Column(name="last_name")
	private String lastName;
	
	@JsonProperty
	@Column(name="phone_number")
	private String phoneNumber;
	
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
