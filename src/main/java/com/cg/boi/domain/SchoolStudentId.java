package com.cg.boi.domain;

import java.io.Serializable;

public class SchoolStudentId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String studentId;
	
	private String courseId;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return studentId.hashCode() + 1234 * courseId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return studentId.equals(((SchoolStudent)obj).getStudentId())
				&& courseId.equals(((SchoolStudent)obj).getCourses());
	}
}
