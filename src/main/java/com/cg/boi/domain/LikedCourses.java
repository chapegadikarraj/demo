package com.cg.boi.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SqlResultSetMapping;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@IdClass(LikedCoursesId.class)
@SqlResultSetMapping(name = "courseLike", entities = {
		@EntityResult(entityClass = LikedCourses.class, fields = {
				@FieldResult(name = "studentId", column = "student_id"),
				@FieldResult(name = "courseId", column = "course_id"),
				@FieldResult(name = "studentName", column = "student_name"),
				@FieldResult(name = "courseName", column = "course_name")
		})
})
public class LikedCourses implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty
	@Id
	private String studentId;
	
	@JsonProperty
	@Id
	private String courseId;

	@JsonProperty
	private String studentName;
	
	@JsonProperty
	private String courseName;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
//	@Override
//	public int hashCode() {
//		// TODO Auto-generated method stub
//		return studentId.hashCode() + 1234 * courseId.hashCode();
//	}
//	
//	@Override
//	public boolean equals(Object obj) {
//		// TODO Auto-generated method stub
//		return studentId.equals(((SchoolStudent)obj).getStudentId())
//				 && courseId.equals(((SchoolStudent)obj).getCourseId());
//	}
	
	


}
