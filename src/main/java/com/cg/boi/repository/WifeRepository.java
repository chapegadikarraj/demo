package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.Cart;
import com.cg.boi.domain.Wife;


@Repository
public interface WifeRepository extends JpaRepository<Wife, String>{
	
	public Wife findByName(String name);
	public List<Wife> findAll(); 

}
