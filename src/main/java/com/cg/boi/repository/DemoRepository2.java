package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.Profile;

@Repository
public interface DemoRepository2 extends JpaRepository<Profile, String>{
	
	public Profile findByNameAndLastName(String name, String lastName);
	public List<Profile> findAll(); 
}
