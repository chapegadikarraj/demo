//package com.cg.boi.splunk;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.splunk.Receiver;
//import com.splunk.Service;
//import com.splunk.ServiceArgs;
//
//@Configuration
//public class SplunkConfiguration {
//	
//	@Value("${splunk.username}")
//	private String username;
//	
//	@Value("${splunk.password}")
//	private String password;
//	
//	@Value("${splunk.host}")
//	private String host;
//	
//	@Value("${splunk.port}")
//	private String port;
//	
//	@Bean
//	public Receiver getReceiver() {
////		HttpService.setSslSecurityProtocol(SSLSecurityProtocol.TLSv1_2);
//		ServiceArgs loginArgs = new ServiceArgs();
//		loginArgs.setUsername(username);
//		loginArgs.setPassword(password);
//		loginArgs.setHost(host);
//		loginArgs.setPort(Integer.parseInt(port));
//		
//		// Create a Service instance and log in with the argument map
//		Service service = Service.connect(loginArgs);
//		return service.getReceiver();
//	}
//}
