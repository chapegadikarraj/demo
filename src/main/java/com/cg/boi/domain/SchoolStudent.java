package com.cg.boi.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="school_student")
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "studentId")
//@Document(collection="profile")
public class SchoolStudent {

	@JsonProperty
	@Id
	@Column(name="student_id")
	private String studentId;

	@JsonProperty
	@Column(name="name")
	private String name;
	
	@JsonProperty
	@Column(name="course_id")
	private String courseId;
	
//	@JsonIgnore
	@ManyToMany
	@JoinTable(
			  name = "course_like", 
			  joinColumns = @JoinColumn(name = "student_id"), 
			  inverseJoinColumns = @JoinColumn(name = "course_id"))
	private Set<Course> courses;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}
	
	
	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return studentId.hashCode() + 1234 * courseId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return studentId.equals(((SchoolStudent)obj).getStudentId())
				 && courseId.equals(((SchoolStudent)obj).getCourseId());
	}

}
