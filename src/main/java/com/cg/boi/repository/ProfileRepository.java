package com.cg.boi.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cg.boi.domain.Cart;
import com.cg.boi.domain.Profile;


@Repository
public interface ProfileRepository extends JpaRepository<Profile, String>{
	
	public Cart findByName(String name);
	public List<Profile> findAll(); 
	
	
	@Modifying
	@Transactional
	@Query(value = "update profile set name = :name where profile_id = :id", nativeQuery = true)
	public void updateProfile(@Param("name") String name, @Param("id") String id);

}
