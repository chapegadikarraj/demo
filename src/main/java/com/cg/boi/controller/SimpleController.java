package com.cg.boi.controller;



import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cg.boi.beans.CourseResponse;
import com.cg.boi.beans.Profile;
import com.cg.boi.beans.ProfileJson;
import com.cg.boi.beans.SchoolStudentResponse;
import com.cg.boi.domain.Cart;
import com.cg.boi.domain.Course;
import com.cg.boi.domain.LikedCourses;
import com.cg.boi.domain.Husband;
import com.cg.boi.domain.Item;
import com.cg.boi.domain.MongoProfile;
import com.cg.boi.domain.SchoolStudent;
import com.cg.boi.domain.Wife;
import com.cg.boi.repository.CartRepository;
import com.cg.boi.repository.LikedCoursesRepository;
import com.cg.boi.repository.ProfileRepository;
import com.cg.boi.repository.CourseRepository;
import com.cg.boi.repository.DemoRepository;
import com.cg.boi.repository.DemoRepository2;
import com.cg.boi.repository.HusbandRepository;
import com.cg.boi.repository.ItemRepository;
import com.cg.boi.repository.SchoolStudentRepository;
import com.cg.boi.repository.WifeRepository;
import com.cg.boi.service.SaveService;


@RestController
public class SimpleController {
	
	
	@Autowired
	private SaveService service;
	
	@Autowired
	private DemoRepository demoRepository; 
	
	@Autowired
	private DemoRepository2 demoRepository2;
	
	@Autowired
	private CartRepository cart;
	
	@Autowired
	private ItemRepository item;
	
	@Autowired
	private HusbandRepository husband;
	
	@Autowired
	private WifeRepository wife;
	
	@Autowired
	private CourseRepository course;
	
	@Autowired
	private LikedCoursesRepository courseLikeRepository;
	
	@Autowired
	private SchoolStudentRepository schoolStudent;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	//@Autowired
	private Profile profile;
	
	public SimpleController() {
		Profile profile= new Profile();
	}
	
	@RequestMapping(value="/welcome",produces="application/xml")
	public Profile welcome(){
		profile = new Profile();
			profile.setName("Raj");
			profile.setLastName("Chapegadikar");
			profile.setPhoneNumber("9420057076");
			return profile;
	}
	
	@RequestMapping(value="/save/{db}", consumes = "application/json", produces="application/json")
	public com.cg.boi.domain.Profile check(@RequestBody com.cg.boi.domain.Profile profile, @PathVariable(name="db") String db){
		
		if("postgres".equals(db)) {
			service.save(profile);
		} else if("mongo".equals(db)) {
			MongoProfile mprofile = new MongoProfile();
			BeanUtils.copyProperties(profile, mprofile);
			mprofile.setId(UUID.randomUUID().toString());
			demoRepository.save(mprofile);
		}
 		return profile;
	}
	
	@RequestMapping(value="/post", consumes = "application/xml",method=RequestMethod.POST, produces = "application/json")
	public ResponseEntity<ProfileJson> postXML(@RequestBody Profile profile){
		
		profile.setLastName("Not Chapegadikar");
		System.out.println(profile.toString());
		return new ResponseEntity<>(new ProfileJson(profile.getName(), profile.getLastName(), profile.getPhoneNumber()), HttpStatus.OK);
	}
	
	@RequestMapping(value="/post", consumes = "application/json", method=RequestMethod.POST, produces = "application/xml")
	public Profile postJson(@RequestBody ProfileJson profile){
		
		profile.setLastName("Not Chapegadikar");
		System.out.println(profile.toString());
		return new Profile(profile.getName(), profile.getLastName(), profile.getPhoneNumber());
	}
	
	@GetMapping(value="/cart", produces = "application/json")
	public ResponseEntity<List<Cart>> getCarts() {
		return ResponseEntity.ok(cart.findAll());
	}
	
	@GetMapping(value="/item", produces = "application/json")
	public ResponseEntity<List<Item>> getItems() {
		return ResponseEntity.ok(item.findAll());
	}
	
	@GetMapping(value="/husband", produces = "application/json")
	public ResponseEntity<List<Husband>> getHusbands() {
		return ResponseEntity.ok(husband.findAll());
	}
	
	@GetMapping(value="/wife", produces = "application/json")
	public ResponseEntity<List<Wife>> getWifes() {
		return ResponseEntity.ok(wife.findAll());
	}
	
	@GetMapping(value="/course", produces = "application/json")
	public ResponseEntity<List<CourseResponse>> getCourses() {
		List<CourseResponse> list = course.findAll().stream().map(x -> {
			CourseResponse response = new CourseResponse();
			BeanUtils.copyProperties((Course)x, response);
			return response;
		}).collect(Collectors.toList());
		return ResponseEntity.ok(list);
	}
	
	@GetMapping(value="/student", produces = "application/json")
	public ResponseEntity<List<SchoolStudentResponse>> getStudents() {
		List<SchoolStudentResponse> list = schoolStudent.findAll().stream().map(x -> {
			SchoolStudentResponse response = new SchoolStudentResponse();
			BeanUtils.copyProperties((SchoolStudent)x, response);
			return response;
		}).collect(Collectors.toList());
		return ResponseEntity.ok(list);
	}
	
	@GetMapping(value="/student/{name}", produces = "application/json")
	public ResponseEntity<List<LikedCourses>> getStudents(@PathVariable(name = "name") String name) {
		return ResponseEntity.ok(courseLikeRepository.findJoin(name));
	}
	
	@GetMapping(value="/profileUpdate/{id}", produces = "application/json")
	public ResponseEntity<String> updateProfile(@PathVariable(name = "id") String id) {
		profileRepository.updateProfile("ravan", id);
		return ResponseEntity.ok("Updated");
	}
	
	
}


