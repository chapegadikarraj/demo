package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.Item;
import com.cg.boi.domain.ItemId;


@Repository
public interface ItemRepository extends JpaRepository<Item, String>{
	
	public Item findByItemId(String itemId);
	public List<Item> findAll(); 

}
