package com.cg.boi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="item")
@IdClass(ItemId.class)
//@Document(collection="profile")
public class Item {

	@JsonProperty
	@Id
	@Column(name="item_id")
	private String itemId;

	@JsonProperty
	@Column(name="name")
	private String name;
	
	@JsonIgnore
	@Id
	@Column(name="cart_id")
	private String cartId;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "cart_id", insertable = false, updatable = false)
	private Cart cart;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}
	
//	@Override
//	public int hashCode() {
//		// TODO Auto-generated method stub
//		return itemId.hashCode();
//	}
//	
//	@Override
//	public boolean equals(Object obj) {
//		// TODO Auto-generated method stub
//		return this.itemId.equals(((Item)obj).getItemId());
//	}

	
}
