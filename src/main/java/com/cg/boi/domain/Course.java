package com.cg.boi.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="course")
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "courseId")
//@Document(collection="profile")
public class Course {

	@JsonProperty
	@Id
	@Column(name="course_id")
	private String courseId;

	@JsonProperty
	@Column(name="name")
	private String name;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "courses")
	private Set<SchoolStudent> students;

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<SchoolStudent> getStudents() {
		return students;
	}

	public void setStudents(Set<SchoolStudent> students) {
		this.students = students;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return courseId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return courseId.equals(((Course)obj).getCourseId());
	}
}
