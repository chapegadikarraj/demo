package com.cg.boi.exception;

import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice(basePackages = {"com.cg.boi.controller"})
public class ExceptionHandler {

//	@Autowired
//	private Receiver receiver;
	
	private static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);
	
	@org.springframework.web.bind.annotation.ExceptionHandler(RuntimeException.class)
	public ResponseEntity<String> sendExceptionMessage(RuntimeException e) {
		LOGGER.error("TimeStamp: " + new Timestamp(new Date().getTime())
				+ " Error: {}", e.getMessage());
//		receiver.log("TimeStamp: " + new Timestamp(new Date().getTime())
//				+ " Error: {}", e.getMessage());
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
	public ResponseEntity<String> sendException(Exception e) {
		LOGGER.error("TimeStamp: " + new Timestamp(new Date().getTime())
				+ " Error: {}", e.getMessage());
//		receiver.log("TimeStamp: " + new Timestamp(new Date().getTime())
//				+ " Error: {}", e.getMessage());
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
