package com.cg.boi.service;

import com.cg.boi.domain.Profile;

public interface SaveService {
	public void save(Profile profile);
}
