package com.cg.boi.domain;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.springframework.data.mongodb.core.mapping.Document;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="profile", propOrder={"profileId","name","lastName","phoneNumber"}) 
@Document(collection="profile")
public class MongoProfile {

	@XmlElement
	@Id
	private String id;

	@XmlElement
	private String name;
	
	@XmlElement
	private String lastName;
	
	@XmlElement
	private String phoneNumber;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
