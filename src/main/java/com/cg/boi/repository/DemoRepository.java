package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.MongoProfile;


@Repository
public interface DemoRepository extends MongoRepository<MongoProfile, String>{
	
	public MongoProfile findByName(String name);
	public MongoProfile findByNameAndLastName(String name, String lastName);
	public List<MongoProfile> findAll(); 

}
