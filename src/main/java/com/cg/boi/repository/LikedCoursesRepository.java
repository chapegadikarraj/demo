package com.cg.boi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cg.boi.domain.LikedCourses;
import com.cg.boi.domain.SchoolStudent;


@Repository
public interface LikedCoursesRepository extends JpaRepository<LikedCourses, String>{

	@Query(value = "select s.student_id, s.course_id, s.name as student_name, c.name as course_name"
			+ " from school_student s"
			+ " join course c"
			+ " on s.course_id = c.course_id where s.name = :name", nativeQuery = true)
	public List<LikedCourses> findJoin(@Param("name") String name);
}
