package com.cg.boi.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="profile", propOrder={"profileId","name","lastName","phoneNumber"}) 
public class Profile {

	@XmlElement
	private String profileId;

	//@Value("Raj")
	@XmlElement
	private String name;
	//@Value("Chapegadikar")
	@XmlElement
	private String lastName;
	//@Value("9420057076")
	@XmlElement
	private String phoneNumber;
	
	public Profile(){
		
	}
	
	public Profile(String name, String lastName, String phoneNumber) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	
	@Override
	public String toString() {
		
		return "name: "+this.getName()+" lastName: "+this.getLastName()+" phoneNumber: "+this.getPhoneNumber();
	}
}
