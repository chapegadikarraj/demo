package com.cg.boi.domain;

import java.io.Serializable;

public class ItemId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String itemId;
	
	private String cartId;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return itemId.hashCode() + 1234 * cartId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return itemId.equals(((ItemId)obj).getItemId())
				&& cartId.equals(((ItemId)obj).getCartId());
	}

}
