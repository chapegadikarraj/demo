package com.cg.boi.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(value = {"profileId", "name", "lastName", "phoneNumber"})
public class ProfileJson {
	
	@JsonProperty
	private String profileId;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private String lastName;

	@JsonProperty
	private String phoneNumber;
	
	public ProfileJson() {
		
	}

	public ProfileJson(String name, String lastName, String phoneNumber) {
		this.name=name;
		this.lastName=lastName;
		this.phoneNumber=phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	@Override
	public String toString() {
		
		return "name: "+this.getName()+" lastName: "+this.getLastName()+" phoneNumber: "+this.getPhoneNumber();
	}
	

}
